package pl.makrohard.shortcutter;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView settingsInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        settingsInfo = (TextView) findViewById(R.id.settings_info);

        if(!checkWriteSecureSettingsPermission()) {
            settingsInfo.setText(Html.fromHtml(getString(R.string.grant_secure)));
        } else {
            if(!Settings.System.canWrite(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intent.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
                startActivity(intent);
            } else {
                settingsInfo.setText(Html.fromHtml(getString(R.string.all_fine)));
            }
        }
    }

    private boolean checkWriteSecureSettingsPermission()
    {
        String permission = "android.permission.WRITE_SECURE_SETTINGS";
        int res = checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
}
