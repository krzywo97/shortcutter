package pl.makrohard.shortcutter;


import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.service.quicksettings.Tile;
import android.service.quicksettings.TileService;

public class ShowTouchesService extends TileService {
    @Override
    public void onClick() {
        super.onClick();
        enableShowTouches(!isShowTouchesEnabled());
        Tile tile = getQsTile();
        tile.setState(isShowTouchesEnabled() ? Tile.STATE_ACTIVE : Tile.STATE_INACTIVE);
        tile.updateTile();
    }

    @Override
    public void onTileAdded() {
        super.onTileAdded();
        Tile tile = getQsTile();
        tile.setState(isShowTouchesEnabled() ? Tile.STATE_ACTIVE : Tile.STATE_INACTIVE);
        tile.updateTile();
    }

    private boolean isShowTouchesEnabled() {
        boolean enabled = false;
        try {
            enabled = (Settings.System.getInt(getContentResolver(), "show_touches") == 1);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return enabled;
    }

    private void enableShowTouches(boolean enable) {
        if(Settings.System.canWrite(getApplicationContext())) {
            /*try {
                Process settings = Runtime.getRuntime().exec("settings get system show_touches");
                BufferedReader reader = new BufferedReader(new InputStreamReader(settings.getInputStream()));

                String line;
                while((line = reader.readLine()) != null) {
                    System.out.print(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }*/
            Settings.System.putInt(getApplicationContext().getContentResolver(), "show_touches", enable ? 1 : 0);
        } else {
            Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
            intent.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
            startActivity(intent);
        }
    }
}
