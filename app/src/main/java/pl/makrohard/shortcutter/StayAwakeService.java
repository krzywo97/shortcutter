package pl.makrohard.shortcutter;


import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.service.quicksettings.Tile;
import android.service.quicksettings.TileService;

public class StayAwakeService extends TileService {
    @Override
    public void onClick() {
        super.onClick();
        enableStayAwake(!isStayAwakeEnabled());
        Tile tile = getQsTile();
        tile.setState(isStayAwakeEnabled() ? Tile.STATE_ACTIVE : Tile.STATE_INACTIVE);
        tile.updateTile();
    }

    @Override
    public void onTileAdded() {
        super.onTileAdded();
        Tile tile = getQsTile();
        tile.setState(isStayAwakeEnabled() ? Tile.STATE_ACTIVE : Tile.STATE_INACTIVE);
        tile.updateTile();
    }

    private boolean isStayAwakeEnabled() {
        boolean enabled = false;
        try {
            enabled = (Settings.Global.getInt(getContentResolver(), Settings.Global.STAY_ON_WHILE_PLUGGED_IN) == 1);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return enabled;
    }

    private void enableStayAwake(boolean enable) {
        Settings.Global.putInt(getApplicationContext().getContentResolver(), Settings.Global.STAY_ON_WHILE_PLUGGED_IN, enable ? 1 : 0);
    }
}
