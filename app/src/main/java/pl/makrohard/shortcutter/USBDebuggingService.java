package pl.makrohard.shortcutter;

import android.provider.Settings;
import android.service.quicksettings.Tile;
import android.service.quicksettings.TileService;

public class USBDebuggingService extends TileService {
    @Override
    public void onClick() {
        super.onClick();
        enableUsbDebugging(!isDebuggingEnabled());
        Tile tile = getQsTile();
        tile.setState(isDebuggingEnabled() ? Tile.STATE_ACTIVE : Tile.STATE_INACTIVE);
        tile.updateTile();
    }

    @Override
    public void onTileAdded() {
        super.onTileAdded();
        Tile tile = getQsTile();
        tile.setState(isDebuggingEnabled() ? Tile.STATE_ACTIVE : Tile.STATE_INACTIVE);
        tile.updateTile();
    }

    private boolean isDebuggingEnabled() {
        boolean enabled = false;
        try {
            enabled = (Settings.Secure.getInt(getContentResolver(), Settings.Secure.ADB_ENABLED) == 1);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return enabled;
    }

    private void enableUsbDebugging(boolean enable) {
        Settings.Secure.putInt(getContentResolver(), Settings.Secure.ADB_ENABLED, enable ? 1 : 0);
    }
}
